Drupal.behaviors.selector_element_jquery = function(context) {

  /* unfortunately Drupal does not scope its forms with a CSS id so there's 
  *  nothing meaningful that we can do to prevent conflicts of similar element 
  *  names across forms :( */
  var elements = Drupal.settings.jquery_selector;
    
  var id;
  
  for (elid in elements) {
    id = elements[elid];
    drupal_jquery_select_element_domap(id);
  }
  
}

/**
* @param selectbox must be a jQuery object!
* Credits: http://www.robbiebow.co.uk/blog/2008/08/add-options-to-a-select-with-j.html
*/
function selector_element_add_option2selectbox(selectbox, val, txt, selected) {
  var optn = document.createElement('option');
  optn.value = val;
  optn.selected = selected;
  optn.appendChild(document.createTextNode(txt));  
  selectbox.append(optn);  
}

function update_selectbox(id, source_ul, type) {
  
    var selectbox;
    //edit-funds-selection-funds-selection-enabled
    var id2 = id.replace( '_', '-');
    selectbox = $('#edit-'+id2+'-'+id2+'-'+type);
    selectbox.empty(); 

    $.each(source_ul.children('li'), function() {
      if (!$(this).hasClass('ui-sortable-helper')) { //make sure it ain't an "in-transit" element that we have to ignore.
        opid = $(this).attr('id');
          opid = opid.replace(id+'_', '');
        opval = $(this).text(); 
        selector_element_add_option2selectbox(selectbox, opid, opval, true);
      }
    });

}

function drupal_jquery_select_element_domap(id) {
  
  //Drupal-translatable CSS style for i18n
  var css_dropzone = Drupal.settings.jquery_selector_dropzone;
  
  $('#' + id + " ul").sortable({
  		
  		dropOnEmpty: true,  
  		
  		connectWith: ['#' + id + " ul"],
  		
  		receive: function(event, ui) {
  		  var children = ui.element.children('li');
  		  if (children.length == 1) {
  		    ui.element.removeClass(css_dropzone);  		
  		  }
  		  var enabled_ul, disabled_ul;
  		  if (ui.element.hasClass('selector_disabled')) {
  		    enabled_ul = ui.sender;
  		    disabled_ul = ui.element;
  		  } else {
  		    enabled_ul = ui.element;
  		    disabled_ul = ui.sender;
  		  }
  		  
  		  update_selectbox(id, enabled_ul, 'enabled');
  		  update_selectbox(id, disabled_ul, 'disabled');  		 

  		},
  		
      change: function(event, ui) {
  		  var enabled_ul, disabled_ul;
  		  if (ui.element.hasClass('selector_disabled')) {
  		    disabled_ul = ui.element;
    		  update_selectbox(id, disabled_ul, 'disabled');  		    
  		  } else {
  		    enabled_ul = ui.element;
    		  update_selectbox(id, enabled_ul, 'enabled');		    
  		  }  		 
  		},
  		  		
  		remove: function(event, ui) {
  		  if (ui.element.children().length == 1) {
  		    ui.element.addClass(css_dropzone);
  		  }
  		}						
  });
}

